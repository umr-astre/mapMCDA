---
title: "Collection and pre-processing of cartographic information for Cameroon"
author: "Facundo Muñoz"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Collection and pre-processing of cartographic information for Cameroon}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

Here we demonstrate how we have downloaded and pre-processed the cartographic
information for Cameroon which is included in the package.


```{r packages}
require(terra)
require(sf)
require(geodata)

if(!require(mapview, quietly = TRUE)) 
  cat("We suggest installing the pacakge mapview for interactive visualisation",
      "of cartography from within R")

```

## Administrative borders

Download cartography from the Global Administrative Borders Database (GADM,
https://gadm.org/) directly from within R.

```{r admin-borders}

cmr_admin3 <- geodata::gadm(
  country = "Cameroon",
  level=3,          # 3rd admin level
  resolution = 2,   # low-res
  path = tempdir()
)
# mapview(cmr_admin3, zcol = "NAME_3")

```

```{r water-bodies, eval = FALSE}

## This only works locally.
prodel_path <- "/home/facu/CmisSync/Cirad/Sites/PRODEL/documentLibrary/carto"
water_bodies <- read_sf(file.path(prodel_path, "wb_cam.shp.shp"))

```

```{r national-parks, eval = FALSE}
national_parks <- read_sf(
  file.path(prodel_path, "WDPA_Mar2018_CMR-shapefile"),
  "WDPA_Mar2018_CMR-shapefile-polygons"
)

```


```{r production-systems, eval = FALSE}
# Not using this for the moment
ps_cam <- terra::rast("ps_cam.tif")

```

```{r animal-density, eval = FALSE}
animal_density_world <- terra::rast(file.path(prodel_path, "glw", "WdCt8k_vf_Mn_Rw_To.tif"))
animal_density <- terra::mask(
  crop(
    animal_density_world,
    terra::ext(cmr_admin3)
  ),
  cmr_admin3
)

# plot(animal_density)
# summary(animal_density$WdCt8k_vf_Mn_Rw_To)

```

## Save pre-processed cartography for use within the package

Prefer standard and modern Open Geospatial Consortium ([OGC](http://www.opengeospatial.org/))
formats: GeoPackage for vector maps and GeoTiff for raster images.

```{r save-package-carto, eval = FALSE}
cmr_dir <- "./inst/cartography/CMR"
dir.create(cmr_dir, recursive = TRUE)

writeVector(cmr_admin3, file.path(cmr_dir, "cmr_admin3.gpkg"))
write_sf(water_bodies, file.path(cmr_dir, "water_bodies.gpkg"), layer = "water_bodies", driver = "GPKG")
write_sf(national_parks, file.path(cmr_dir, "national_parks.gpkg"), layer = "national_parks", driver = "GPKG")

terra::writeRaster(animal_density, file.path(cmr_dir, "animal.density.tif"))


```

