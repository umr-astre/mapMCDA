# mapMCDA 0.5

* Update back-end geographical packages (sf, terra)

  - Remove dependency on deprecated packages sp, raster, rgdal, rgeos, maptools and maps.

* Use more suitable default colour scales based on viridis

* Include the package installation location in the dropdown menu of locations

  - Provide direct access to Cameroon example data
  


# mapMCDA 0.4


* Read network data. 

  - Filter national sub-network automatically (with warning) rather than stop on nodes off-boundaries. (#44)

  - Warn (instead of error) if all links are unidirectional. Danger: assuming directionality when in reality the data is non-directional. (#41)
  
  - Automatically aggregate volumes for multiple links (with warning,
  rather than error).
  
  - Stricter check consistency of nodes coordinates. Before, we
  checked (and errored) whether the same node had different
  coordinates. Check also for the same coordinates havind different
  nodes. It is likely a typo in the name of a locality, and we don't
  want to treat them as different nodes.
  
* Accept grd/gri files as raster format.

* New vignette: animal mobility file-format cheatsheet (https://umr-astre.pages.mia.inra.fr/mapMCDA/articles/animal_mobility.html).

* shiny interface v2 with several improvements (#8)

* several bug- and doc-fixes


# mapMCDA 0.3.0

* Plot geographic networks (#13)

* Export CSV with units and risk categories (#10)

* Animal mobility data as a specific input type (#9)


# mapMCDA 0.2.0

* Fully working version


# mapMCDA 0.1.0

* First working prototype

* Added a `NEWS.md` file to track changes to the package.



