#' Compute a suitable resolution for a map
#' 
#' Compute a resolution in the original map units that conforms to given
#' criteria.
#' 
#' 
#' @param x a SpatRaster or sf or sfc object
#' @param max_ncells Number of cells for the largest dimension
#'
#' @return A number. The resolution that respects the desired number of cells.
#'
#' @import terra
#' @export
#' @examples
#'   r <- terra::rast(nrows = 5, ncols = 11, xmin = 0, xmax = 11, ymin = 0, ymax = 5)
#'   resolution(r, max_ncells = 10)
resolution <- function(x, max_ncells = 100) {
  
  if (inherits(x, "sfc")) x <- st_sf(x)
  
  ext <- terra::ext(x)
  span_x <- terra::xmax(ext) - terra::xmin(ext)
  span_y <- terra::ymax(ext) - terra::ymin(ext)
  
  max_span <- max(span_x, span_y)
  res <- max_span/max_ncells
  return(res)
}
