#' Load a map layer
#'
#' Automatically detects whether it is a vector or raster map and uses the
#' appropriate read function.
#' 
#' @param x File name with full path
#'
#' @return A \code{sf} object for vector maps and a \code{SpatRaster}
#'   object for raster maps.
#'   
#' @export
#' @import terra
#' @import sf
#'
#' @examples
#'  (pkg_layers <- list.files(
#'    system.file("cartography/CMR", package = "mapMCDA"),
#'    full.names = TRUE
#'  ))
#'  
#'  epiunits <- load_layer(pkg_layers[[2]])
load_layer <- function(x) {

  lt <- layer_type(basename(x))
  if (is.na(lt)) return(invisible(NULL))
  
  load_f <- c(vector = "read_sf", raster = "rast", network = "read_network")
  
  return(do.call(load_f[lt], list(x)))
}


#' Load all layers within a directory
#' 
#' Load all recognised vector and raster files within a given directory.
#'
#' @param path String. The path to the directory where spatial layers are
#'   stored.
#'   
#' @return list of layers in the form of sf or SpatRaster objects
#' @export
#'
#' @examples
#'    pkg_layers <- system.file(path = "cartography/CMR/", package = "mapMCDA")
#'    cmr <- load_dir(pkg_layers)
load_dir <- function(path) {
  
  filenames <- list.files(path, full.names = TRUE)
  names(filenames) <- rmext(basename(filenames))
  layers <- lapply(filenames, load_layer)
  
  ## remove unknown (NULL) layers
  return(layers[!vapply(layers, is.null, TRUE)])
  
}