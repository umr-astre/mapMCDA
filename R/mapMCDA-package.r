#' mapMCDA
#' 
#' Produce an epidemiological risk map by weighting multiple risk factors.
#' 
#' The typical use case is the lack of (reliable) data about the epidemics of a
#' disease, but available information about well known risk factors (e.g. animal
#' density, proximity to border, proximity to water courses, etc.). In order to
#' design a stratified sampling or a surveillance campaign, a preliminar risk
#' map based on expert judgement is required. This package (and method) provides
#' a systematic and guided approach to build such maps.
#' 
#' mapMCDA provides a Shiny graphical user interface launched with
#' \code{mapMCDA_app()}.
#' It can also be used from command line and scripted.
#' You can see an example (in french) in the vignette 
#' \href{https://umr-astre.pages.mia.inra.fr/mapMCDA/articles/mapMCDA.html}{Overview}.
#' 
#' 
#' @references https://umr-astre.pages.mia.inra.fr/mapMCDA/
#'
#' @name mapMCDA
#' @import methods
#' @keywords internal 
"_PACKAGE"

.onLoad <- function(libname, pkgname) {
  mapMCDA_app()
}