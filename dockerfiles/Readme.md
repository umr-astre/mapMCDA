# Try mapMCDA

Run mapMCDA in a Docker container, mapping the current directory into the directory `ext` in the container _home_.

In Windows:

    docker run -d --rm -p 3838:3838 --name mapmcda -v ${PWD}:/root/ext registry.forgemia.inra.fr/umr-astre/mapmcda/mapmcda
  
In Linux/MacOS:

    docker run -d --rm -p 3838:3838 --name mapmcda -v $(pwd):/root/ext registry.forgemia.inra.fr/umr-astre/mapmcda/mapmcda

Navigate to `localhost:3838` in a web browser.

After finishing working with mapMCDA, run

    docker kill mapmcda
    
to stop the container.


# Testing environments for mapMCDA

Under `test_environment` we store Dockerfiles defining three computing environments
for testing mapMCDA:

- `oldrel`: the latest major release of R,

- `release`: the current release of R

- `devel`: the next major release, under development

The images are built locally (see Makefile) and pushed to the [Docker registry
at Forgemia](https://forgemia.inra.fr/umr-astre/mapMCDA/container_registry).

These images are used for Continuous Integration testing ([`.gitlab-ci.yml`](https://forgemia.inra.fr/umr-astre/mapMCDA/-/blob/master/.gitlab-ci.yml)).

The images are based on the [Rocker-versioned
images](https://hub.docker.com/r/rocker/r-ver) which in turn are based on either
Debian (for R < 4.0) or Ubuntu (for R >= 4.0).

For `oldrel`, package dependencies are installed from a CRAN snapshot at the
last date that particular R version was current. The result is an old R version
with contemporary versions of R-packages (i.e. not latest versions).


# Run

You can experiment with a container using the development version of R with
all needed mapMCDA dependencies from both the system and R-packages.


    docker run --rm -ti registry.forgemia.inra.fr/umr-astre/mapmcda/mapmcda_testenv:devel

