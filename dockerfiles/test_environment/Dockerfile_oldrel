FROM rocker/r-ver:4.2.3

LABEL maintainer="Facundo Muñoz facundo.munoz@cirad.fr"

ENV CI_PROJECT_DIR="/builds/umr-astre/mapMCDA"

# Install external dependencies
RUN export DEBIAN_FRONTEND=noninteractive; apt-get -qq update \
  && apt-get install -y --no-install-recommends \
	libv8-dev \
	libjq-dev \
	libharfbuzz-dev \
	libfribidi-dev \
	libprotobuf-dev \
	protobuf-compiler \
	libfontconfig1-dev \
	gdal-bin \
	libgdal-dev \
	libglpk-dev \
	libgmp-dev \
	libproj-dev \
	libxml2-dev \
	libudunits2-dev \
	libcairo2-dev \
	pandoc \
	qpdf \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/

# Install R-package dependencies for running mapMCDA
RUN ["install2.r", "deldir", "geodata", "geonetwork", "igraph", "rhandsontable", "shiny", "shinycssloaders", "shinydashboard", "shinyFiles", "sf", "terra"]

# Install R-packages dependencies for building mapMCDA
RUN ["install2.r", "devtools", "covr", "lintr", "knitr", "lwgeom", "mapview", "rmarkdown", "roxygen2", "testthat"]

# Clean up
RUN rm -rf /tmp/downloaded_packages/ /tmp/*.rds

# Copy mapMCDA source directory
# COPY . $CI_PROJECT_DIR

# Build package
# Don't build. That is part of the check.
# RUN R CMD build /builds/umr-astre/mapMCDA

# Set up environment
WORKDIR $CI_PROJECT_DIR
RUN echo 'alias ll="ls -lh --color=tty"' >> ~/.bashrc

CMD ["R"]
