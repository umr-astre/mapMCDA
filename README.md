
<!-- badges: start -->

[![Project Status: Active – The project has reached a stable, usable
state and is being actively
developed.](https://www.repostatus.org/badges/latest/active.svg)](https://www.repostatus.org/#active)
[![CRAN_Status_Badge](http://www.r-pkg.org/badges/version/mapMCDA)](https://cran.r-project.org/package=mapMCDA)
[![UMR ASTRE
R-universe](https://cirad-astre.r-universe.dev/badges/:name)](https://cirad-astre.r-universe.dev)
[![mapMCDA R-universe status
badge](https://cirad-astre.r-universe.dev/badges/mapMCDA)](https://cirad-astre.r-universe.dev/mapMCDA)
[![Build
status](https://forgemia.inra.fr/umr-astre/mapMCDA/badges/master/pipeline.svg)](https://forgemia.inra.fr/umr-astre/mapMCDA/pipelines)
[![Appveyor Build
status](https://ci.appveyor.com/api/projects/status/5p751lpceeiy18kp?svg=true)](https://ci.appveyor.com/project/famuvie/mapmcda)
<!-- badges: end -->

# mapMCDA <img src="man/figures/logo.png" align="right" />

![](man/figures/mapMCDA_overview.png)

`mapMCDA` helps producing **risk maps** by weighting **risk factors**
using **expert knowledge**.

The typical use case is the lack of (reliable) data about the epidemics
of a disease, but available information about well known **risk
factors** (e.g. animal density, proximity to border, proximity to water
courses, etc.).

In order to design a **stratified sampling** or a **surveillance
campaign**, a preliminary risk map based on expert judgement is
required.

This package (and method) provides a systematic and guided approach to
build such maps.

![](man/figures/plot-risk-unit-1.png)

The package include a Graphical User Interface (Shiny) that helps in
processing and weighting risk factors.

``` r
library(mapMCDA)
mapMCDA_app()
```

![](man/figures/interface.png)

## Installing the package

`mapMCDA` is on its way to CRAN. In the meanwhile, install from our
r-universe as follows

<!-- To install the current stable, CRAN version of the package, type: -->
<!-- ```{r install, eval = FALSE} -->
<!-- install.packages("mapMCDA") -->
<!-- ``` -->
<!-- To benefit from the latest features and bug fixes, install the development, version of the package using: -->
<!--
&#10;```r
if (!require("remotes")) {
  install.packages("remotes")
}
remotes::install_gitlab("umr-astre/mapMCDA", host = "forgemia.inra.fr")
```
-->

``` r
# Install mapMCDA in R:
install.packages('mapMCDA', repos = c('https://cirad-astre.r-universe.dev', 'https://cloud.r-project.org'))
```

<!-- # Resources -->
<!-- ## Vignettes -->
<!-- An overview and examples of *mapMCDA* are provided in the vignettes: -->
<!-- ... -->
<!-- ## Websites -->
<!-- The following websites are available: -->
<!-- ... -->

## Getting help online

Bug reports and feature requests should be posted on *GitLab* using the
[*issue*](http://forgemia.inra.fr/umr-astre/mapMCDA/issues) system.

For support, reach out in the [mapMCDA mailing
list](https://listes.cirad.fr/sympa/info/mapmcda). Archives are of
public access.

Contributions are welcome via **pull requests**.

Please note that this project is released with a [Contributor Code of
Conduct](CONDUCT.md). By participating in this project you agree to
abide by its terms.
