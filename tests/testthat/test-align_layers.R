context("align_layers")

animald <- mapMCDA_datasets()$animal.density

test_that("handle rasters without projection information", {
  
  x <- list(animald, animald)
  crs(x[[1]]) <- NA
  expect_equal(
    length(unique(lapply(align_layers(x), crs))),
    1L
  )
  
})

test_that("harmonise rasters with different projections", {
  
  ## Albers Equal Area Africa 1 CRS
  ## http://spatialreference.org/ref/sr-org/8476/
  proj.srorg8476 <- "+proj=aea +lat_1=36.5 +lat_2=29.071428571429 +lat_0=32.7857142857145 +lon_0=-14.111111111111 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs"
  adp <- terra::project(animald, crs(proj.srorg8476))
  aligned <- align_layers(list(animald, adp, animald))
  expect_identical(
    unique(vapply(aligned, crs, character(1))),
    crs(animald)
  )
})

test_that("harmonise resolution and extents", {
  
  #' All rasters are resampled to the same extent and resolution as the \emph{first}
  #' raster in the list.

  tr <- list(
    terra::rast(nrows = 10, ncols = 10, xmin = 0, xmax = 9, ymin = 0, ymax = 9),
    terra::rast(nrows = 11, ncols = 11, xmin = 1, xmax = 10, ymin = 1, ymax = 10)
  )
  
  expect_error(ar <- align_layers(tr), NA)
  expect_identical(resolution(ar[[1]]), resolution(ar[[2]]))
  expect_identical(resolution(tr[[1]]), resolution(ar[[2]]))
  expect_equal(terra::ext(ar[[1]]), terra::ext(ar[[2]]))
  expect_equal(terra::ext(tr[[1]]), terra::ext(ar[[2]]))
})
